<!doctype html>
<html lang="fr">
<head>
    <!-- CodeColliders 2020 - Exercice PHP -->
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tableaux PHP - Partie 2 - Exercices 1 à 5</title>
</head>
<body>
<h1>Exercices Tableaux PHP</h1>
<?php
/**
 * Cours-Exercices sur les tableaux PHP
 *
 * Bienvenue dans la deuxième partie des exercices sur les tableaux PHP.
 *
 * Nous allons maintenant aborder la notion de tableau associatif, où on associe les valeurs à des clés.
 *
 * Exemple :
 */
$voiture = [
    "couleur" => "rouge",
    "puissance" => "33CV",
    "marque" => "Tata",
    "modele" => "Nano",
];

/**
 * pour accéder à une valeur du tableau, on utilise sa clé de la façon suivante:
 * $voiture["couleur"]; // ce code retourne la valeur contenue dans le tableau et ayant pour clé "couleur"
 * de la même façon, à l'aide de la fonction echo(), on peut afficher cette valeur :
 * echo $voiture["couleur"]; // ce code affiche la valeur contenue dans le tableau et ayant pour clé "couleur"
 */

/**
 * Exercice 1:
 * Afficher ci-dessous dans la page la marque de la voiture.
 * Utiliser une balise H3.
 */
echo '<h2>Exercice 1</h2>';

echo '<h3>'.$voiture["marque"].'</h3>';

/**
 * Exercice 2:
 * Ajouter au tableau $voiture la valeur "105km/h" ayant pour clé "vitesseMax".
 * Afficher le contenu du tableau.
 */
echo '<h2>Exercice 2</h2>';

$voiture["vitesseMax"] = "105km/h";


/**
 * Exercice 3:
 * Le tableau $notes contient la liste des notes de Math d'une classe de 6ème.
 * Afficher $notes sous forme d'un tbleau HTML (<table> ...) contenant les prénoms et les notes, à l'aide d'une boucle foreach.
 * N'hésitez pas à vous aider de la documentation PHP sur foreach pour récupérer les clés et les valeurs !
 */
echo '<h2>Exercice 3</h2>';
$notes = [
    'Simon' => 10,
    'Jeanne' => 8,
    'Marion' => 14,
    'Anaïs' => 6,
    'Rémi' => 4,
    'Éléonore' => 16,
    'Vincent' => 19,
    'Théo' => 12,
    'François' => 15,
    'Lola' => 11,
];

echo "<table>";
foreach ($notes as $key => $value) {
    echo "<tr><td>$key</td><td>$value</td></tr>";
}
echo "<table>";

/**
 * Exercice 4:
 * Trier le tableau $notes par ordre décroissant (les meilleurs élèves en haut de la liste)
 * Attention chaque élève doit garder sa note !
 * Afficher à nouveau le tableau.
 * Aidez-vous de l'une de ses fonctions:
 * https://www.php.net/manual/fr/function.sort.php
 * https://www.php.net/manual/fr/function.asort.php
 * https://www.php.net/manual/fr/function.rsort.php
 * https://www.php.net/manual/fr/function.arsort.php
 * https://www.php.net/manual/fr/function.ksort.php
 * https://www.php.net/manual/fr/function.krsort.php
 */
echo '<h2>Exercice 4</h2>';

arsort($notes);
echo "<table>";
foreach ($notes as $key => $value) {
    echo "<tr><td>$key</td><td>$value</td></tr>";
}
echo "<table>";


/**
 * Exercice 5:
 * Trier le tableau $notes par prénom, dans l'ordre alphabétique.
 * Attention chaque élève doit garder sa note !
 * Afficher à nouveau le tableau.
 */
echo '<h2>Exercice 5</h2>';

ksort($notes);

echo "<table>";
foreach ($notes as $key => $value) {
    echo "<tr><td>$key</td><td>$value</td></tr>";
}
echo "<table>";




$notes = [
    'Simon' => 10,
    'Jeanne' => 8,
    'Marion' => 14,
    'Anaïs' => 6,
    'Rémi' => 4,
    'Éléonore' => 16,
    'Vincent' => 19,
    'Théo' => 12,
    'François' => 15,
    'Lola' => 11,
];

/**
 * Exercice 6:
 * A l'aide d'une boucle et d'une condition, afficher un tableau HTML contenant la liste des élèves et leurs
 * notes, pour les élèves ayant une note supérieure ou égale à 10.
 */
echo '<h2>Exercice 6</h2>';

echo "<table>";
foreach ($notes as $key => $value) {
    if ($value >= 10)
        echo "<tr><td>$key</td><td>$value</td></tr>";

echo "<table>";
}


$chars = "azertyuiopmlkjhgfdsqxwcvbnAZERTYUIOPMLKJHGFDSQXWCVBN?:;.+-*";
$password = '';
$length = 16;

/**
 * Exercice 7:
 * Une chaine de caractères pêut être utilisée comme un tableau de caractères.
 * exemple : echo $chars[1]; // affiche la lettre z
 *
 * Générer dans la variable $password un mot de passe de longueur $length
 * utilisant au hasard les caractères présents dans la chaine $chars
 * ps: un même charactère peut être utilisé plusieurs fois
 * utiliser la fonction mt_rand() : https://www.php.net/manual/fr/function.mt-rand.php
 */
echo '<h2>Exercice 7</h2>';


for ($i=0;$i<$length;$i++) {
    $index = mt_rand(0,(strlen($chars)-1));
    //echo "$index - ";
    $password.= $chars[$index];
}

echo 'Mot de passe : ' . $password;


$resultats = [
    "Simon" => [
        "age" => 25, // en années
        "tempsCoures" => 30, // en minutes
    ],
    "Jean" => [
        "age" => 35, // en années
        "tempsCoures" => 45, // en minutes
    ],
    "Claire" => [
        "age" => 19, // en années
        "tempsCoures" => 90, // en minutes
    ],
    "Anne-Lise" => [
        "age" => 18, // en années
        "tempsCoures" => 68, // en minutes
    ],
    "Lucie" => [
        "age" => 43, // en années
        "tempsCoures" => 35, // en minutes
    ],
    "Bernard" => [
        "age" => 50, // en années
        "tempsCoures" => 40, // en minutes
    ],
];

/**
 * Exercice 8:
 * Le tableau $resultats contient les résultats d'une course à pied. Calculer et afficher le temps de course moyen pour
 * les participants ayant plus de 30 ans.
 */
echo '<h2>Exercice 8</h2>';

$arr = array();



echo "<table>";
foreach ($resultats as $person_key => $person_value_arr) {
    if ($person_value_arr["age"] > 30)
        echo "<tr><td>$person_key</td><td>".$person_value_arr["age"]."</td></tr>";
}

?>
</body>
</html>