<!doctype html>
<html lang="fr">
<head>
    <!-- CodeColliders 2020 - Exercice PHP -->
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tableaux PHP - Exercices 1 à 7</title>
</head>
<body>
<h1>Exercices Tableaux PHP</h1>
<?php
/**
 * Cours-Exercices sur les tableaux PHP
 *
 * il y a deux façon d'instancier une variable de type tableau (array) :
 * $variable = array();
 * ou
 * $variable = [];
 */
$joursSemaine = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

/**
 * Voici une variable de type tableau contenant les 7 jours de la semaine.
 * Le tableau contient ici 7 valeurs ayant pour index de 0 à 6.
 * Exemple :
 * la valeur "Dimanche" du tableau $joursSemaine a pour index 0
 * la valeur "Mercredi" du tableau $joursSemaine a pour index 3
 *
 * pour accéder à une valeur du tableau, on utilise son index de la façon suivante:
 * $joursSemaine[0]; // ce code retourne la valeur du tableau contenue en position 0
 * de la même façon, à l'aide de la fonction echo(), on peut afficher cette valeur :
 * echo $jourSemaine[0]; // ce code affiche la valeur du tableau contenue en position 0
 */

/**
 * Exercice 1:
 * Afficher ci-dessous dans la page le jour de la semaine ayant pour index 5.
 * Utiliser une balise HTML paragraphe.
 */
echo '<h2>Exercice 1</h2>';
echo "<p>$joursSemaine[5]</p>";

/**
 * Exercice 2:
 * Grâce à la fonction date(), on récupère dans la variable $indexAujourdhui l'index de 0 à 6 du jour actuel de la semaine.
 * Utiliser la fonction echo(), le tableau $joursSemaine et la variable $indexAujourdhui pour afficher le jour actuel.
 * Utiliser une balise HTML paragraphe.
 */
echo '<h2>Exercice 2</h2>';
$indexAujourdhui = date("w");
echo $joursSemaine[$indexAujourdhui];

/**
 * Exercice 3:
 * Pour vérifier ce que contient un tableau, on peut utiliser les fonctions print_r(); ou var_dump();
 * https://www.php.net/manual/fr/function.print-r.php
 * https://www.php.net/manual/fr/function.var-dump.php
 * A l'aide d'une de ces deux fonctions, afficher le contenu du tableau $joursSemaine dans une balise HTML pre.
 */
echo '<h2>Exercice 3</h2>';

echo "<pre>";
print_r($joursSemaine);
echo "</pre>";

/**
 * Exercice 4:
 * Les weekends ne sont pas assez longs !!
 * Dans le tableau $joursSemaine, remplacer la valeur "Lundi" par "Encore dimanche", à l'aide de son index et en
 * utilisant un opérateur d'affectation.
 * exemple de syntaxe: $monTableau[123] = "Nouvelle valeur"; // affecte la valeur "Nouvelle valeur" à l'index 123
 * du tableau $monTableau
 */
echo '<h2>Exercice 4</h2>';

$joursSemaine[1] = "Encore Dimanche";

/**
 * Exercice 5:
 * Répéter l'exercice 3 pour vérifier que l'exercice 4 est correctement réalisé.
 */
echo '<h2>Exercice 5</h2>';

echo "<pre>";
print_r($joursSemaine);
echo "</pre>";

/**
 * Exercice 6:
 * A l'aide d'une boucle for, parcourir le tableau et afficher l'ensemble de ses valeurs dans une liste à puces (balises
 * HTML ul et li).
 * Vous pouvez utiliser la fonction count() pour compter le nombre d'element d'un tableau.
 */
echo '<h2>Exercice 6</h2>';

echo '<ul>';
for ($i=0;$i<count($joursSemaine);$i++)
    echo "<li>$joursSemaine[$i]</li>";
echo '</ul>';

/**
 * Exercice 7:
 * Répétez l'exercice 6 à l'aide d'une boucle foreach :
 * foreach fournit une façon simple de parcourir des tableaux et permet de répéter l'execution d'une portion de code
 * pour chaque valeur d'un tableau
 *   foreach ($monTableau as $maValeur){
 *     // code à répéter
 *   }
 * pour en savoir plus: https://www.php.net/manual/fr/control-structures.foreach.php
 */
echo '<h2>Exercice 7</h2>';

echo '<ul>';
foreach ($joursSemaine as $day)
    echo "<li>$day</li>";
echo '</ul>';

/**
 * Cours-Exercices sur les tableaux PHP
 */

$nombres = [0, 1, 2, 3];

/**
 * Exercice 8:
 * la variable nombre contient un tableau ayant pour valeurs 0, 1, 2 et 3
 * ajouter la valeur 4 au tableau contenu dans la variable $nombres
 * exemple de syntaxe : $monTableau[] = 'nouvelle valeur';
 * afficher le contenu de la variable $nombres pour contrôler le résultat (cf. exercice 3)
 */
echo '<h2>Exercice 8</h2>';

$nombres[] = 4;
echo "<pre>";
print_r($nombres);
echo "</pre>";


/**
 * Exercice 9:
 * Fusioner la variable $autreNombres de type tableau à la variable $nombre de type tableau à l'aide de la fonction
 * array_merge(): https://www.php.net/manual/fr/function.array-merge.php
 * afficher le contenu de la variable $nombres pour contrôler le résultat (cf. exercice 3)
 */
echo '<h2>Exercice 9</h2>';
$autresNombres = [5, 6, 7, 8, 9];

echo array_merge($nombres,$autresNombres);

echo "<pre>";
print_r($nombres);
echo "</pre>";

/**
 * Exercice 10:
 * Il existe de nombreuses fonctions en PHP pour manipuler les tableaux :
 * https://www.php.net/manual/fr/book.array.php
 *
 * Afin de commencer à mettre en pratique certaines de ces fonctions:
 *
 * 1/ afficher la somme des elements du tableau $nombres à l'aide de la fonction array_sum()
 * 2/ afficher le nombre d'element dans le tableau $nombre à l'aide de la fonction count()
 * 3/ à l'aie de la fonction sort() trier le tableau $nombres par ordre décroissant, puis l'afficher.
 */
echo '<h2>Exercice 10</h2>';


echo '<p> Somme = '.array_sum($nombres).'</p>';
echo '<p> Compte = '.count($nombres).'</p>';
rsort($nombres);
echo "<pre>";
print_r($nombres);
echo "</pre>";


$tableDe = 4;
$nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

/**
 * Exercice 11:
 * à l'aide de la boucle foreach parcourir le tableau $nombres pour afficher la table de multiplication de la valeur
 * contenue dans la variable $tableDe.
 * Chaque ligne du résultat généré doit ressembler à l'extrait suivant : "4 * 1 = 4"
 */
echo '<h2>Exercice 11</h2>';

foreach ($nombres as $nombre) {
    $result = $tableDe * $nombre;
    echo "<p>$tableDe * $nombre = $result</p>";
}


$tableDe = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

/**
 * Exercice 12:
 * à l'aide de boucles foreach, parcourir le tableau $nombres pour afficher la table de multiplication de chacune des
 * valeurs contenues dans le tableau $tableDe
 * Chaque ligne du résultat généré doit ressembler à l'extrait suivant : "6 * 2 = 12"
 */
echo '<h2>Exercice 12</h2>';

foreach ($tableDe as $mult) {
    foreach ($nombres as $nombre) {
        $result = $mult * $nombre;
        echo "<p>$mult * $nombre = $result</p>";
    }
}



$statistiquesJournalieres = [222, 293, 210, 123, 171, 147, 204, 246, 257, 204, 165, 236, 287, 120, 294, 206, 162, 211,
    262, 195, 202, 154, 115, 178, 200, 114, 113, 256, 216, 278, 113, 211, 272, 106, 251, 146, 129, 293, 125, 250, 268,
    278, 231, 110, 123, 233, 293, 134, 101, 129, 300, 202, 296, 285, 147, 129, 290, 263, 193, 189, 112, 263, 283, 105,
    227, 225, 165, 221, 289, 141, 298, 172, 125, 119, 277, 165, 147, 183, 151, 214, 277, 250, 190, 181, 121, 100, 161,
    164, 104, 269, 278, 276, 244, 170, 143, 158, 217, 283, 227, 220, 281, 166, 224, 292, 214, 182, 164, 174, 197, 294,
    205, 192, 291, 143, 128, 174, 234, 203, 255, 256, 137, 136, 292, 275, 153, 228, 165, 248, 170, 240, 152, 185, 108,
    258, 269, 199, 153, 144, 126, 272, 123, 179, 163, 174, 229, 208, 300, 184, 111, 243, 162, 299, 269, 241, 250, 122,
    255, 204, 256, 166, 230, 189, 220, 152, 195, 184, 135, 270, 113, 224, 266, 153, 161, 287, 267, 186, 261, 151, 204,
    184, 118, 263, 226, 247, 286, 246, 134, 184, 156, 239, 173, 152, 271, 123, 295, 178, 243, 260, 128, 116, 157, 132,
    297, 236, 186, 274, 260, 252, 261, 151, 240, 141, 224, 209, 204, 240, 163, 197, 170, 174, 264, 115, 294, 100, 202,
    175, 201, 249, 275, 280, 205, 184, 186, 182, 124, 305, 250, 197, 203, 247, 245, 227, 105, 188, 164, 178, 117, 235,
    212, 208, 242, 281, 292, 228, 181, 224, 183, 115, 215, 233, 175, 118, 137, 158, 231, 246, 295, 291, 297, 272, 149,
    291, 249, 103, 232, 201, 235, 279, 148, 180, 148, 266, 118, 189, 297, 263, 116, 268, 118, 156, 220, 172, 111, 157,
    163, 181, 152, 197, 186, 237, 213, 128, 149, 248, 228, 161, 111, 135, 122, 214, 136, 136, 258, 260, 209, 177, 257,
    103, 273, 236, 203, 151, 134, 207, 143, 297, 153, 270, 230, 172, 183, 224, 214, 194, 141, 190, 134, 230, 167, 204,
    118, 285, 246, 105, 131, 198, 243, 184, 129, 169, 176, 228, 286, 124, 99, 116, 159, 188, 264, 247, 125, 241, 228,
    290, 197];

/**
 * Exercice 13:
 * Le tableau $statistiquesJournalieres ci-dessus contient le nombre de visiteur journalier d'un site Internet pour les
 * 365 derniers jours. A l'aide de PHP, déterminer et afficher :
 * - la valeur la plus grande
 * - la valeur la plus petite
 * - le total cumulé du nombre de visiteur sur les 365 derniers jours
 * - la moyenne journalière (calcul à réaliser: nombre total de visiteur divisé par le nombre de jours)
 */
echo '<h2>Exercice 13</h2>';

sort($statistiquesJournalieres);
$lastindex = end($statistiquesJournalieres);
echo "<p> La valeur la plus grande = $lastindex</p>";
echo "<p> La valeur la plus petite = $statistiquesJournalieres[0]</p>";
$sum = array_sum($statistiquesJournalieres);
$moyenne = $sum / 365;
echo "<p>le total cumulé du nombre de visiteur sur les 365 derniers jours = $sum</p>";
echo "<p>la moyenne journalière = $moyenne</p>";




$lettres = ['P', 'm', 'd', '7', '.', 'a', '2', 'T', ';', '\'', '!', 'u', 'h', 'q', ',', '/', 'g', 'N', '<', '"', 'i', 'C', 'n', 'b', '=', 's', 'f', '0', 'x', 'p', 'c', 'L', 'r', 'F', 'l', '&', '>', ':', 'H', 'o', 't', '-', 'v', 'M', 'B', 'e', ' '];
$message = [18, 29, 36, 33, 35, 45, 5, 30, 11, 40, 45, 8, 34, 20, 30, 20, 40, 5, 40, 20, 39, 22, 25, 46, 18, 23, 36, 21, 12, 5, 1, 29, 20, 39, 22, 4, 22, 45, 18, 15, 23, 36, 14, 46, 40, 11, 46, 5, 25, 46, 40, 45, 32, 1, 20, 22, 35, 45, 5, 30, 11, 40, 45, 8, 46, 34, 5, 46, 29, 32, 45, 1, 20, 35, 45, 16, 32, 5, 42, 45, 8, 32, 45, 46, 25, 35, 45, 5, 30, 11, 40, 45, 8, 32, 20, 45, 46, 2, 9, 45, 28, 45, 32, 30, 20, 30, 45, 25, 46, 25, 11, 32, 46, 34, 45, 25, 46, 40, 5, 23, 34, 45, 5, 11, 28, 46, 0, 38, 0, 10, 18, 15, 29, 36, 18, 29, 36, 43, 5, 20, 25, 46, 20, 34, 46, 22, 45, 46, 25, 9, 5, 16, 20, 25, 25, 5, 20, 40, 46, 13, 11, 45, 46, 2, 9, 11, 22, 45, 46, 1, 20, 25, 45, 46, 45, 22, 46, 23, 39, 11, 30, 12, 45, 4, 18, 15, 29, 36, 18, 29, 36, 31, 5, 46, 25, 11, 20, 40, 45, 46, 2, 45, 25, 46, 45, 28, 45, 32, 30, 20, 30, 45, 25, 46, 25, 45, 46, 40, 32, 39, 11, 42, 45, 46, 35, 5, 16, 32, 5, 42, 45, 8, 46, 34, 9, 5, 2, 32, 45, 25, 25, 45, 46, 25, 11, 20, 42, 5, 22, 40, 45, 46, 37, 46, 18, 5, 46, 12, 32, 45, 26, 24, 19, 12, 40, 40, 29, 25, 37, 15, 15, 16, 20, 40, 12, 11, 23, 4, 30, 39, 1, 15, 17, 20, 5, 1, 39, 32, 3, 27, 15, 7, 5, 23, 34, 45, 5, 11, 28, 41, 0, 38, 0, 41, 41, 41, 0, 5, 32, 40, 20, 45, 41, 6, 19, 36, 12, 40, 40, 29, 25, 37, 15, 15, 16, 20, 40, 12, 11, 23, 4, 30, 39, 1, 15, 17, 20, 5, 1, 39, 32, 3, 27, 15, 7, 5, 23, 34, 45, 5, 11, 28, 41, 0, 38, 0, 41, 41, 41, 0, 5, 32, 40, 20, 45, 41, 6, 18, 15, 5, 36, 18, 15, 29, 36, 18, 29, 36, 44, 39, 22, 22, 45, 46, 30, 12, 5, 22, 30, 45, 46, 10, 18, 15, 29, 36];

/**
 * Exercice 14: Message codé
 * Le tableau $lettres contient toutes les lettres utilisées dans le message codé.
 * Le tableau $message contient, dans l'ordre des lettres du message codé, les index des lettres dans le tableau $lettres.
 * à vous d'afficher le message !
 */
echo '<h2>Exercice 14</h2>';

foreach ($message as $mess) {
    echo $lettres[$mess];
}



?>

<a href="ex-1-8.php">Partie 2</a>

</body>
</html>
